#include "Tile.h"



Tile::Tile(int x, int y, int tileType)
{
	collider.pos.x = x;
	collider.pos.y = y;
	//Set the collision box
	collider.w = TILE_WIDTH;
	collider.h = TILE_HEIGHT;

	//Get the tile type
	type = tileType;
	
}

void Tile::show(SDL_Surface* screen)
{
	//Display texture
	tile.spritesheet->apply_surface((int)(collider.pos.x), (int)(collider.pos.y), tile.spritesheet->getSurface(), screen, &tile.tile[type]);
}

int Tile::get_type()
{
	return type;
}


AABB Tile::getAABB()
{
	return collider;
}

Tile::~Tile()
{

}

bool Tile::set_tiles(Tile * tiles[])
{
	//The tile offsets
	int x = 0, y = 0;

	//Open the map
	std::ifstream map("lazy.map");



	//Initialize the tiles
	for (int t = 0; t < TOTAL_TILES; t++)
	{
		//Determines what kind of tile will be made
		int tileType = -1;

		//Read tile from map file
		map >> tileType;

		//If the was a problem in reading the map
		if (map.fail() == true)
		{
			//Stop loading map
			map.close();
			return false;
		}

		//If the number is a valid tile number
		if ((tileType >= 0) && (tileType < TILE_SPRITES))
		{
			tiles[t] = new Tile(x, y, tileType);
		}
		//If we don't recognize the tile type
		else
		{
			//Stop loading map
			map.close();
			return false;
		}

		//Move to next tile spot
		x += TILE_WIDTH;

		//If we've gone too far
		if (x >= 640)
		{
			//Move back
			x = 0;

			//Move to the next row
			y += TILE_HEIGHT;
		}
	}

	//Close the file
	map.close();

	//If the map was loaded fine
	return true;
}

bool Tile::touches_wall(AABB box, Tile * tiles[])
{
	//Go through the tiles
	for (int t = 0; t < TOTAL_TILES; t++)
	{
		//If the tile is a wall type tile
		if (tiles[t]->get_type() ==0 )
		{
			//If the collision box touches the wall tile
			if (AABB::intersectAABB(&box, &tiles[t]->getAABB()) == true)
			{
				return true;
			}
		}
	}

	//If no wall tiles were touched
	return false;
}

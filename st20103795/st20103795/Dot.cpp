#include "Dot.h"



Dot::Dot(float x, float y)
{
	collider.pos.x = x;
	collider.pos.y = y;
	collider.h = 32;
	collider.w = 32;
}


Dot::~Dot()
{

}

void Dot::show(SDL_Surface * screen)
{
	dot.spritesheet->apply_surface(collider.pos.x, collider.pos.y, dot.spritesheet->getSurface(), screen, &dot.object[1]);
}

AABB Dot::getAABB()
{
	return collider;
}

#pragma once
#include"Clips.h"
#include"AABB.h"
class Dot
{
	Clips dot;
	AABB collider;
public:
	Dot(float x, float y);
	~Dot();
	void show(SDL_Surface* screen);
	AABB getAABB();
};


#pragma once

#include <cmath>
#include <SDL.h>
#include "Point.h"

class AABB {



public:


	Point		pos;

	
	int		w;
	int		h;

	
	AABB(Point pos, int w, int h) {

		this->pos = pos;
		this->w = w;
		this->h = h;
	}
	AABB() {}

	void setPosition(Point newPos) {

		pos = newPos;
	}

	// Return true if two AABBs intersect, false otherwise
	static bool intersectAABB(AABB* a, AABB* b) {

		//The sides of the rectangle
		float leftA, leftB;
		float rightA, rightB;
		float topA, topB;
		float bottomA, bottomB;

		//Calculate the sides of rectangle A
		leftA = a->pos.x;
		rightA = a->pos.x + a->w;
		topA = a->pos.y;
		bottomA = a->pos.y + a->h;

		//Calculate the sides of rectangle B
		leftB = b->pos.x;
		rightB = b->pos.x + b->w;
		topB = b->pos.y;
		bottomB = b->pos.y + b->h;

		//If any edge of rectangle A is outside of rectangle B
		if (bottomA <= topB)
		{
			return false;
		}

		if (topA >= bottomB)
		{
			return false;
		}

		if (rightA <= leftB)
		{
			return false;
		}

		if (leftA >= rightB)
		{
			return false;
		}

		//If there isn't an edge outside of rectangle B
		return true;
	}

	static bool intersectAABB2(AABB* a, AABB* b) {

		//The sides of the rectangle
		float leftA, leftB;
		float rightA, rightB;
		float topA, topB;
		float bottomA, bottomB;

		//Calculate the sides of rectangle A
		leftA = a->pos.x;
		rightA = a->pos.x + a->w;
		topA = a->pos.y;
		bottomA = a->pos.y + a->h;

		//Calculate the sides of rectangle B
		leftB = b->pos.x;
		rightB = b->pos.x + b->w;
		topB = b->pos.y;
		bottomB = b->pos.y + b->h;

		//If any edge of rectangle A is outside of rectangle B
		if (bottomA < topB)
		{
			return false;
		}

		if (topA > bottomB)
		{
			return false;
		}

		if (rightA < leftB)
		{
			return false;
		}

		if (leftA > rightB)
		{
			return false;
		}

		//If there isn't an edge outside of rectangle B
		return true;
	}
};

#include "Game.h"





Game::Game()
{
	
}


Game::~Game()
{
	
	freeMemory();
	//Quit SDL_ttf
	TTF_Quit();

	Mix_CloseAudio();


	//Quit SDL
	SDL_Quit();
	

}

bool Game::start()
{
	if (init() == false)
	{
		return false;
	}
	//Set the tiles
	if (Tile::set_tiles(tiles) == false)
	{
		return false;
	}

	player = new Player();

	box[0] = new Box(128, 128);
	box[1] = new Box(128, 256);
	box[2] = new Box(384, 256);
	box[3] = new Box(448, 256);

	dot[0] = new Dot(80, 80);
	dot[1] = new Dot(80, 336);
	dot[2] = new Dot(400, 80);
	dot[3] = new Dot(528, 336);

	message[0] = new TTF("victory", "lazy.ttf", 40, 0, 0, 0);
	message[1]= new TTF("failure", "lazy.ttf", 40, 0, 0, 0);
	

	restart[0] = new Button(300, 330, "restart.png", 0, 0, 0);
	restart[1] = new Button(0, 0, "restart.png", 0, 0, 0);
	delta.start();
	timer.start();
	music = new Music();
	
	if (gameLoop()==false)
	{
		return false;
	}
	
	return true;
}






bool Game::init()
{
	//Initialize all SDL subsystems
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		return false;
	}
	
	if (TTF_Init() == -1)
	{
		return false;
	}

	
	//Set up the screen
	window = new Screen();


	//If there was an error in setting up the screen
	if (window == NULL)
	{
		return false;
	}

	//Set the window caption
	SDL_WM_SetCaption(title, NULL);

	//If everything initialized fine
	return true;
}

bool Game::gameLoop()
{
	//Quit flag
	quit = false;
	

	//While the user hasn't quit
	while (quit == false)
	{

		//When there are events that need to be processed
		while (SDL_PollEvent(&event))
		{
			//Dealing with player events
			player->handle_input(event);

			//if the user closes the window
			if (event.type == SDL_QUIT)
			{
				//exit the program
				quit = true;
			}

			restart[1]->handle_events(event);
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				//Get mouse coordinates
				int x = event.button.x;
				int y = event.button.y;
				if ((x > restart[1]->getRect().x) && (x < restart[1]->getRect().x + restart[1]->getRect().w) && (y > restart[1]->getRect().y) && (y < restart[1]->getRect().y + restart[1]->getRect().h))
				{

					//If the left mouse button is pressed
					if (event.button.button == SDL_BUTTON_LEFT)
					{
						Mix_PlayChannel(-1, music->clic, 0);
						freeMemory();
						start();
					}
				}
			}
		}


		//Move player
		player->move(delta.get_ticks(), box, tiles);

		for (int i = 0; i < 4; i++)
		{
			box[i]->move(player, box, tiles, delta.get_ticks());
		}

		//Restart timer
		delta.start();

		//Show the tiles
		for (int t = 0; t < 70; t++)
		{
			tiles[t]->show(window->getSurface());
		}

		for (int i = 0; i < 4; i++)
		{
			dot[i]->show(window->getSurface());
		}

		for (int i = 0; i < 4; i++)
		{
			box[i]->show(window->getSurface());
		}

		//Show the player on the screen
		player->show(window->getSurface(), delta);
		restart[1]->show(window->getSurface());

		//String to hold time
		std::stringstream time;

		//Convert the timer's time to a string
		time << "Timer: " << (int)(120-(timer.get_ticks() / 1000.f));

		message[2] = new TTF(time.str().c_str(), "lazy.ttf", 40, 0, 0, 0);
		
		//Application time surface
		message[2]->apply_surface((640-message[2]->getSurface()->w)/2,0, message[2]->getSurface(),window->getSurface());
		//Update the screen
		if (SDL_Flip(window->getSurface()) == -1)
		{
			return false;
		}
		delete message[2];

		while ((int)(timer.get_ticks() / 1000.f) >= 120)
		{
			SDL_FillRect(window->getSurface(), &window->getSurface()->clip_rect, SDL_MapRGB(window->getSurface()->format, 0xFF, 0xFF, 0xFF));
			message[1]->apply_surface((640 - message[1]->getSurface()->w) / 2, 100, message[1]->getSurface(), window->getSurface());
			restart[0]->show(window->getSurface());
			SDL_Event event;
			if (SDL_PollEvent(&event))
			{
				//When the user closes the window

				if (event.type == SDL_QUIT)
				{
					return true;
				}
				restart[0]->handle_events(event);

				restart[0]->show(window->getSurface());
				if (event.type == SDL_MOUSEBUTTONDOWN)
				{
					//Get mouse coordinates
					int x = event.button.x;
					int y = event.button.y;
					if ((x > restart[0]->getRect().x) && (x < restart[0]->getRect().x + restart[0]->getRect().w) && (y > restart[0]->getRect().y) && (y < restart[0]->getRect().y + restart[0]->getRect().h))
					{
						//If the left mouse button is pressed
						if (event.button.button == SDL_BUTTON_LEFT)
						{
							Mix_PlayChannel(-1, music->clic, 0);
							freeMemory();
							start();
						}
					}
				}
			}
			player->getClip().spritesheet->apply_surface(299, 210, player->getClip().spritesheet->getSurface(), window->getSurface(), &player->getRect()[1]);
			if (SDL_Flip(window->getSurface()) == -1)
			{
				return false;
			}
		}
		
		while (win())
		{
			SDL_FillRect(window->getSurface(), &window->getSurface()->clip_rect, SDL_MapRGB(window->getSurface()->format, 0xFF, 0xFF, 0xFF));
			message[0]->apply_surface((640 - message[0]->getSurface()->w) / 2, 100, message[0]->getSurface(), window->getSurface());
			restart[0]->show(window->getSurface());
			SDL_Event event;
			if (SDL_PollEvent(&event))
			{
				//When the user closes the window
				if (event.type == SDL_QUIT)
				{
					return true;
				}
				restart[0]->handle_events(event);

				restart[0]->show(window->getSurface());
				if (event.type == SDL_MOUSEBUTTONDOWN)
				{
					//Get mouse coordinates
					int x = event.button.x;
					int y = event.button.y;
					if ((x > restart[0]->getRect().x) && (x < restart[0]->getRect().x + restart[0]->getRect().w) && (y > restart[0]->getRect().y) && (y < restart[0]->getRect().y + restart[0]->getRect().h))
					{
						//If the left mouse button is pressed
						if (event.button.button == SDL_BUTTON_LEFT)
						{
							Mix_PlayChannel(-1, music->clic, 0);
							freeMemory();
							start();
						}
					}
				}
			}
			player->getClip().spritesheet->apply_surface(299,210, player->getClip().spritesheet->getSurface(), window->getSurface(), &player->getRect()[1]);
			if (SDL_Flip(window->getSurface()) == -1)
			{
				return false;
			}
		}
	}
	return true;
}




Screen * Game::getScreen()
{
	return window;
}

bool Game::win()
{
	if (Box::touches_box(dot[0]->getAABB(), box)&& Box::touches_box(dot[1]->getAABB(), box) && Box::touches_box(dot[2]->getAABB(), box) && Box::touches_box(dot[3]->getAABB(), box))
	{
		return true;
	}
	
	return false;
}

bool Game::getQuit()
{
	return quit;
}

void Game::freeMemory()
{
	
	for (int i = 0; i < 4; i++)
	{
		delete dot[i];
	}
	for (int i = 0; i < 2; i++)
	{
		delete message[i];
	}
	for (int i = 0; i < 2; i++)
	{
		delete restart[i];
	}


	for (int t = 0; t < 70; t++)
	{
		delete tiles[t];
	}
	delete player;
	
	delete window;
}






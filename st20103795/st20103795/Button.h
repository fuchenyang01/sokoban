#pragma once
#include"Image.h"

class Button:public Image
{
private:
	SDL_Rect box;


public:
	
	~Button();
	
	Button(float x, float y, char * filename, Uint8 R, Uint8 G, Uint8 B);

	//Handle the event and set the button's sprite diagram
	void handle_events(SDL_Event event);

	//Display the button in the window
	void show(SDL_Surface*screen);
	SDL_Rect getRect();
};


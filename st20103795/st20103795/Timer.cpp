#include "Timer.h"
#include <SDL.h>


Timer::Timer()
{
	//Initialize various variables
	startTicks = 0;
	pausedTicks = 0;
	paused = false;
	started = false;
}


Timer::~Timer()
{
}

void Timer::start()
{
	//Start timer
	started = true;

	//Set the timer to non-paused
	paused = false;


	//Get the current clock time
	startTicks = SDL_GetTicks();

}

void Timer::stop()
{
	//Stop timer
	started = false;
	//Set the timer to non-paused
	paused = false;
}

void Timer::pause()
{
	//If the timer is running and not paused
	if ((started==true)&&(paused==false))
	{
		//Pause timer
		paused = true;
		//Calculate the number of ticks when paused
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

void Timer::unpause()
{
	//If the timer is paused
	if (paused==true)
	{
		//Cancel timer pause
		paused = false;

		//Reset start time
		startTicks = SDL_GetTicks() - pausedTicks;

		//Reset pause time
		pausedTicks = 0;
	}
}

int Timer::get_ticks()
{
	//If the timer is running
	if (started==true)
	{
		//If the timer is paused
		if (paused==true)
		{
			//Returns the time saved when the timer was paused
			return pausedTicks;
		}
		else
		{
			//Returns the difference between the current time minus the startup time
			return SDL_GetTicks() - startTicks;
		}
	}
	//If the timer is not running
	return 0;
}

bool Timer::is_started()
{
	return started; ;
}

bool Timer::is_paused()
{
	return  paused;
}

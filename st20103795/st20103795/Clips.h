#pragma once
#include"Image.h"

class Clips
{
	
	
public:
	Image* spritesheet;
	//The areas of the sprite sheet

	int wall = 0;
	int floor = 1;
	int box = 0;
	int dot = 1;

	SDL_Rect clipsRight[3];
	SDL_Rect clipsLeft[3];
	SDL_Rect clipsUp[3];
	SDL_Rect clipsDown[3];
	SDL_Rect tile[2];
	SDL_Rect object[2];

	Clips();
	~Clips();
	void set_clips();
	SDL_Rect* getTile();
};


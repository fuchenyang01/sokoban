#pragma once
#include"AABB.h"
#include"Clips.h"
#include <fstream>

class Tile
{
private:
	AABB collider;
	//The tile type
	int type;
	Clips tile;
	

	//Tile constants
	static const int TILE_WIDTH = 64;
	static const int TILE_HEIGHT = 64;
	static const int TOTAL_TILES = 70;
	static const int TILE_SPRITES = 2;

public:
	Tile(int x, int y, int tileType);
	//Shows the tile
	void show(SDL_Surface* screen);
	//Get the tile type
	int get_type();
	//Get the collision box
	AABB getAABB();
	~Tile();

	static bool set_tiles(Tile *tiles[]);
	static bool touches_wall(AABB box, Tile *tiles[]);
};


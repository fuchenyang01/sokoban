#pragma once
class Point
{

public:

	float	x, y;


	// Default constructor - initialise x and y to 0.0f
	Point();

	~Point();

	Point(const float initX, const float initY);


	float getX();
	float getY();
	void setX(const float value);
	void setY(const float value);

};


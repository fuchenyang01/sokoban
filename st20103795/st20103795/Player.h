#pragma once
#include"Point.h"
#include <SDL.h>
#include"Image.h"
#include"Screen.h"
#include "Clips.h"
#include"Tile.h"
#include"Box.h"
#include"Timer.h"



class Player;

class Behaviour {

public:

	virtual void move(Player *player, Box* box[], Tile *tiles[], Uint32 deltaTicks)= 0;
	
};

// Player states
enum PlayerState {

	MovingLeft = 0,
	MovingRight = 1,
	MovingUp,
	MovingDown
};


class Player
{
	int index;
	Clips sprite;
	SDL_Rect*  player[4];
	Behaviour	*states[4] = { nullptr, nullptr , nullptr , nullptr};
	PlayerState	currentState;
	const float VEL = 100;
	float a = 0;
	
	//Player's velocity components in the X and Y directions
	float xVel, yVel;
	AABB collider;

	

	class MovingLeft : public Behaviour {

		void move(Player *player, Box* box[], Tile *tiles[], Uint32 deltaTicks);
		
	};

	class MovingRight : public Behaviour {

		void move(Player *player, Box* box[], Tile *tiles[], Uint32 deltaTicks);
		
	};

	class  MovingUp : public Behaviour {

		void move(Player *player, Box* box[], Tile *tiles[], Uint32 deltaTicks);
		
	};

	class MovingDown : public Behaviour {

		void move(Player *player, Box* box[], Tile *tiles[], Uint32 deltaTicks);
		
	};
public:
	
	
	//Initialize variables
	Player();

	~Player();

	//Accept keyboard input and change player speed
	void handle_input(SDL_Event event);

	//change player position
	void move(Uint32 deltaTicks, Box* box[], Tile *tiles[]);

	//display player
	void show(SDL_Surface* screen, Timer delta);

	AABB getAABB();
	void  setAABB(AABB box);
	float getxVel();
	float getyVel();
	void setCurrentState(PlayerState newState);
	PlayerState	getCurrentState();
	Clips getClip();
	SDL_Rect* getRect();
	
};


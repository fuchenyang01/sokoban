#include "TTF.h"



TTF::TTF()
{
	
}

TTF::TTF(const char* message,char* name, int size, Uint8 R, Uint8 G, Uint8 B)
{
	ttfname = name;
	ptsize = size;
	setFont(ttfname,ptsize);
	if (font == NULL)
	{
		printf("font fail");
	}
	Color = { R,G,B };
	setTTF(message);
}


TTF::~TTF()
{

	//Close the font
	TTF_CloseFont(getFont());
}

void TTF::setColor(Uint8 R, Uint8 G, Uint8 B)
{
	Color = { R,G,B };
}

SDL_Color TTF::getColor()
{
	return Color;
}



TTF_Font* TTF::getFont()
{
	return font;
}

void TTF::setFont(char * ttfname, int ptsize)
{
	font = TTF_OpenFont(ttfname, ptsize);
}



void TTF::setTTF(const char* message)
{
	setSurface(TTF_RenderText_Solid(font, message, Color));
}



char* TTF::getTTFname()
{
	return ttfname;
}

void  TTF::setTTFname(char * name)
{
	ttfname=name;
}

int TTF::getPtsize()
{
	return ptsize;
}

void TTF::setPtsize(int size)
{
	ptsize = size;
}







#include "Clips.h"



Clips::Clips()
{
	spritesheet = new Image("sprites.png", 0, 0, 0);
	set_clips();
}


Clips::~Clips()
{

}

void Clips::set_clips()
{
	clipsRight[1].x = 320;
	clipsRight[1].y = 245;
	clipsRight[1].w = 42;
	clipsRight[1].h = 58;

	clipsRight[0].x = 320;
	clipsRight[0].y = 128;
	clipsRight[0].w = 42;
	clipsRight[0].h = 58;

	clipsRight[2].x = 320;
	clipsRight[2].y = 245;
	clipsRight[2].w = 42;
	clipsRight[2].h = 58;

	clipsLeft[0].x = 320;
	clipsLeft[0].y = 304;
	clipsLeft[0].w = 42;
	clipsLeft[0].h = 58;

	clipsLeft[1].x = 320;
	clipsLeft[1].y = 186;
	clipsLeft[1].w = 42; 
	clipsLeft[1].h = 58;

	clipsLeft[2].x = 320;
	clipsLeft[2].y = 304;
	clipsLeft[2].w = 42;
	clipsLeft[2].h = 58;


	clipsUp[0].x = 362;
	clipsUp[0].y = 128;
	clipsUp[0].w = 37;
	clipsUp[0].h = 60;

	clipsUp[1].x = 384;
	clipsUp[1].y = 0;
	clipsUp[1].w = 37;
	clipsUp[1].h = 60;

	clipsUp[2].x = 362;
	clipsUp[2].y = 188;
	clipsUp[2].w = 37;
	clipsUp[2].h = 60;

	clipsDown[0].x = 320;
	clipsDown[0].y = 362;
	clipsDown[0].w = 37;
	clipsDown[0].h = 59;

	clipsDown[1].x = 362;
	clipsDown[1].y = 248;
	clipsDown[1].w = 37;
	clipsDown[1].h = 60;

	clipsDown[2].x = 357;
	clipsDown[2].y = 361;
	clipsDown[2].w = 37;
	clipsDown[2].h = 60;

	tile[wall].x = 0;
	tile[wall].y = 320;
	tile[wall].w = 64;
	tile[wall].h = 64;

	tile[floor].x = 128;
	tile[floor].y = 0;
	tile[floor].w = 64;
	tile[floor].h = 64;
	
	object[box].x = 192;
	object[box].y = 0;
	object[box].w = 64;
	object[box].h = 64;

	object[dot].x = 0;
	object[dot].y = 384;
	object[dot].w = 32;
	object[dot].h = 32;
}

SDL_Rect* Clips::getTile()
{
	return tile;
}

#pragma once
#include<SDL.h>

class Surface
{
	SDL_Surface* surface;
public:
	Surface();
	~Surface();

	
	SDL_Surface* getSurface();
	void setSurface(SDL_Surface* surface);
	void apply_surface(int x, int y, SDL_Surface * source, SDL_Surface * destination, SDL_Rect * clip = NULL);
	
};


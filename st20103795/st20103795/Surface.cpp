#include "Surface.h"



Surface::Surface()
{
	surface = NULL;
}


Surface::~Surface()
{
	//Free surface
	SDL_FreeSurface(surface);
}

SDL_Surface * Surface::getSurface()
{
	return surface;
}

void Surface::setSurface(SDL_Surface * surface)
{
		this->surface = surface;
}

void Surface::apply_surface(int x, int y, SDL_Surface * source, SDL_Surface * destination, SDL_Rect * clip)
{
	//Holds offsets
	SDL_Rect offset;

	//Get offsets
	offset.x = x;
	offset.y = y;

	//Blit
	SDL_BlitSurface(source, clip, destination, &offset);
}





#pragma once

#include<SDL_image.h>
#include<string>

#include"Surface.h"
class Image: public Surface
{
private:
	const char* filename;


public:
	Image(const char* filename);
	Image(const char* filename, Uint8 R, Uint8 G, Uint8 B);
	Image();
	
	~Image();

	void setFilename(const char* filename);
	const char* getFilename();

	SDL_Surface *load_image(const char* filename, Uint8 R, Uint8 G, Uint8 B);
	SDL_Surface* load_image(const char* filename);


	void setImage(const char* filename);
	void setImage(const char* filename, Uint8 R, Uint8 G, Uint8 B);



	
	
};


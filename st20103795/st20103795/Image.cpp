#include "Image.h"





Image::Image(const char* filename )
{
	this->filename = filename;
	setImage(filename);
	
	
}

Image::Image(const char * filename, Uint8 R, Uint8 G, Uint8 B)
{
	this->filename = filename;
	setImage(filename, R, G, B);
	
}

Image::Image()
{
}


Image::~Image()
{

}

void Image::setFilename(const char* filename)
{
	this->filename = filename;
}

const char* Image::getFilename()
{
	return filename;
}

SDL_Surface * Image::load_image(const char* filename, Uint8 R, Uint8 G, Uint8 B)
{
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized surface that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load(filename);

	//If the image loaded
	if (loadedImage != NULL)
	{
		//Create an optimized surface
		optimizedImage = SDL_DisplayFormat(loadedImage);

		//Free the old surface
		SDL_FreeSurface(loadedImage);

		//If the surface was optimized
		if (optimizedImage != NULL)
		{
			//Color key surface
			SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB(optimizedImage->format, R, G, B));
		}
	}

	else
	{
		printf("Failed to load image");
		exit(1);
	}

	//Return the optimized surface
	return optimizedImage;
}

SDL_Surface * Image::load_image(const char* filename)
{
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized surface that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load(filename);

	//If the image loaded
	if (loadedImage != NULL)
	{
		//Create an optimized surface
		optimizedImage = SDL_DisplayFormat(loadedImage);

		//Free the old surface
		SDL_FreeSurface(loadedImage);

		
	}
	
	return optimizedImage;
}











void Image::setImage(const char* filename)
{
	setSurface(load_image(filename));
}

void Image::setImage(const char* filename, Uint8 R, Uint8 G, Uint8 B)
{
	setSurface (load_image(filename, R, G, B));
}


#pragma once
class Timer
{
private:
	//The time when the timer starts
	int startTicks;

	//Number of ticks saved when the timer is paused
	int pausedTicks;

	//Timer status
	bool paused;
	bool started;
public:
	//Constructor
	Timer();

	//Destructor
	~Timer();

	//Various clock actions
	void start();
	void stop();
	void pause();
	void unpause();

	//Get time
	int get_ticks();

	//Check timer status
	bool is_started();
	bool is_paused();
};


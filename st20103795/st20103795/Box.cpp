#include "Box.h"
#include "Player.h"




Box::Box(float x, float y)
{
	collider.pos.x = x;
	collider.pos.y = y;
	collider.h = 64;
	collider.w = 64;
}


Box::~Box()
{

}

void Box::show(SDL_Surface* screen)
{
	box.spritesheet->apply_surface(collider.pos.x, collider.pos.y, box.spritesheet->getSurface(), screen, &box.object[0]);
}

void Box::move(Player * player,Box* box[], Tile*tiles[], Uint32 deltaTicks)
{
	float Vel = 100;
	AABB p = player->getAABB();

	
	//Push from bottom to top
	if (AABB::intersectAABB2( &p,&collider)&& collider.pos.y+63<p.pos.y)
	{
		collider.pos.y -= 64;
		
		if (Tile::touches_wall(collider, tiles) || box_box(box))
		{
		
			collider.pos.y += 64;
			
		}
		//Play sound
		Mix_PlayChannel(-1, music.box, 0);
		
	}
	//From top to bottom
	if (AABB::intersectAABB2(&collider, &p) && collider.pos.y-58>p.pos.y)
	{
		collider.pos.y += 64;
		
		if (Tile::touches_wall(collider, tiles) || box_box(box))
		{

			collider.pos.y -= 64;
			
		}
		Mix_PlayChannel(-1, music.box, 0);
	}
	//From left to right
	if (AABB::intersectAABB2(&collider, &p) && collider.pos.x-41>p.pos.x)
	{
		collider.pos.x +=64;
		
		if (Tile::touches_wall(collider, tiles) || box_box(box))
		{

			collider.pos.x -= 64;
		
		}
		Mix_PlayChannel(-1, music.box, 0);
	}
	//Push from right to left
	if (AABB::intersectAABB2(&collider, &p) && collider.pos.x+63<p.pos.x)
	{
		collider.pos.x -=64;
		
		if (Tile::touches_wall(collider, tiles) || box_box(box))
		{

			collider.pos.x += 64;
			
		}
		Mix_PlayChannel(-1, music.box, 0);
	}
}

AABB Box::getAABB()
{
	return collider;
}

bool Box::notSameBox(AABB collider,Box * box[])
{
	for (int i = 0; i < 4; i++)
	{

		if (box[i]->getAABB().pos.y == collider.pos.y&&box[i]->getAABB().pos.x == collider.pos.x)
		{
			return false;
		}

	}
	return true;
}

bool Box::touches_box(AABB collider, Box * box[])
{
	//Go through the tiles
	for (int t = 0; t < 4; t++)
	{
	
			//If the collision box touches the wall tile
			if (AABB::intersectAABB(&collider, &box[t]->getAABB()) == true)
			{
				return true;
			}
		
	}

	return false;
}

bool Box::box_box(Box * box[])
{
	//Go through the tiles
	for (int t = 0; t < 4; t++)
	{
		if (box[t]!=this)
		{
			if (AABB::intersectAABB(&collider, &box[t]->getAABB()))
			{
				return true;
			}
		}
		
	}
	return false;
}





#include"Game.h"

int main(int argc, char* args[])
{
	//Initialize SDL_mixer
	Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
	
	BGM bgm;
	Mix_PlayMusic(bgm.bgm, -1);
	Game *game=new Game();

	

	if (game->start() == false)
	{
		return 1;
	}
	
	delete game;


	printf("GameOver\n");
	return 0;
}
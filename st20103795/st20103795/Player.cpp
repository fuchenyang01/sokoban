#include "Player.h"



Player::Player()
{
	collider.pos.x = 256;
	collider.pos.y = 256;
	collider.w = 42;
	collider.h = 59;
	index = 1;
	//��ʼ������
	xVel = 0;
	yVel = 0;

	player[PlayerState::MovingLeft] =sprite.clipsLeft;
	player[PlayerState::MovingRight] = sprite.clipsRight;
	player[PlayerState::MovingUp] = sprite.clipsUp;
	player[PlayerState::MovingDown] = sprite.clipsDown;
	
	
	// Create all state objects
	states[PlayerState::MovingLeft] = new Player::MovingLeft();
	states[PlayerState::MovingRight] = new Player::MovingRight();
	states[PlayerState::MovingUp] = new Player::MovingUp();
	states[PlayerState::MovingDown] = new Player::MovingDown();
	

	// Set initial state
	currentState = PlayerState::MovingDown;
}


Player::~Player()
{

}

void Player::handle_input(SDL_Event event)
{

	//When a key is pressed
	if (event.type == SDL_KEYDOWN)
	{
		//Adjust speed
		switch (event.key.keysym.sym)
		{
		case SDLK_UP: yVel -= VEL; break;
		case SDLK_DOWN: yVel += VEL; break;
		case SDLK_LEFT: xVel -= VEL; break;
		case SDLK_RIGHT: xVel += VEL; break;
		}
	}
	else if (event.type == SDL_KEYUP)
	{
		//Adjust speed
		switch (event.key.keysym.sym)
		{
		case SDLK_UP: yVel = 0; break;
		case SDLK_DOWN: yVel = 0; break;
		case SDLK_LEFT: xVel = 0; break;
		case SDLK_RIGHT: xVel = 0; break;
		}
	}

	}

void Player::move(Uint32 deltaTicks, Box* box[], Tile *tiles[])
{
	states[currentState]->move(this,box, tiles ,deltaTicks);
}




void Player::show(SDL_Surface* screen, Timer delta)
{
	
		if (currentState == PlayerState::MovingDown || currentState == PlayerState::MovingUp || currentState == PlayerState::MovingRight || currentState == PlayerState::MovingLeft)
		{
			
			a += 4*(delta.get_ticks() / 1000.f);
			index=(int)a;

		}
	
	if (index > 2)
	{
		index = 0;
		a = 0;
	}
	if (xVel == 0 && yVel == 0)
	{
		index = 1;
	}
	
	
	sprite.spritesheet->apply_surface((int)collider.pos.x, (int)collider.pos.y, sprite.spritesheet->getSurface(), screen, &player[currentState][index]);
	
}

AABB Player::getAABB()
{
	return collider;
}

void Player::setAABB(AABB box)
{
	this->collider = box;
}



float Player::getxVel()
{
	return xVel;
}

float Player::getyVel()
{
	return yVel;
}



void Player::setCurrentState(PlayerState newState)
{
	currentState = newState;
}

PlayerState Player::getCurrentState()
{
	return currentState;
}

Clips Player::getClip()
{
	return sprite;
}

SDL_Rect * Player::getRect()
{
	return player[PlayerState::MovingDown];
}

void Player::MovingLeft::move(Player * player,Box* box[], Tile *tiles[],Uint32 deltaTicks)
{
	AABB collider = player->getAABB();
	float xVel = player->getxVel();
	float yVel = player->getyVel();
	int WIDTH = player->collider.w;
	int HEIGHT = player->collider.h;

	bool changeState = false;
	PlayerState newState;
	//If the player moves to the left
	if (xVel < 0)
	{
		collider.pos.x += xVel*(deltaTicks / 1000.f);
		//If the player moves too far to the left
		if (collider.pos.x < 0)
		{
			//Move back
			collider.pos.x = 0;
		}
		if (Tile::touches_wall(collider, tiles) )
		{
			collider.pos.x -= xVel*(deltaTicks / 1000.f);
		}
		for (int t = 0; t < 4; t++)
		{

			//If the collision box touches the box
			if (AABB::intersectAABB(&collider, &box[t]->getAABB())&& collider.pos.x> box[t]->getAABB().pos.x)
			{
				collider.pos.x = box[t]->getAABB().pos.x + 64;
			}

		}
		
	}

	
	//If the player moves to the right
	if (xVel > 0)
	{
		changeState = true;
		newState = PlayerState::MovingRight;
	}

	if (xVel == 0)
	{
		//If the player moves  Up
		if (yVel < 0)
		{
			changeState = true;
			newState = PlayerState::MovingUp;
		}
		//If the playermoves down
		if (yVel > 0)
		{
			changeState = true;
			newState = PlayerState::MovingDown;
		}
	}

	player->setAABB(collider);

	if (changeState == true) {

		player->setCurrentState(newState);
	}
}

void Player::MovingRight::move(Player * player, Box* box[], Tile *tiles[], Uint32 deltaTicks)
{
	AABB collider = player->getAABB();
	float xVel = player->getxVel();
	float yVel = player->getyVel();
	int WIDTH = player->collider.w;
	int HEIGHT = player->collider.h;

	bool changeState = false;
	PlayerState newState;
	
	//If the player moves to the right
	if (xVel > 0)
	{
		
		collider.pos.x += xVel*(deltaTicks / 1000.f);
		//Or move too far to the right
		if (collider.pos.x + WIDTH > Screen::SCREEN_WIDTH)
		{
			//move back
			collider.pos.x = Screen::SCREEN_WIDTH - WIDTH;
		}
		if (Tile::touches_wall(collider, tiles) )
		{
			collider.pos.x -= xVel*(deltaTicks / 1000.f);
		}
		for (int t = 0; t < 4; t++)
		{

			//If the collision box touches the box
			if (AABB::intersectAABB(&collider, &box[t]->getAABB())&& collider.pos.x<box[t]->getAABB().pos.x)
			{
				collider.pos.x = box[t]->getAABB().pos.x - collider.w;
			}

		}
			
		
	}
	
	//If the dot moves to the left
	if (xVel < 0)
	{
		changeState = true;
		newState = PlayerState::MovingLeft;
	}
	if (xVel == 0)
	{
		//If the player moves up
		if (yVel < 0)
		{
			changeState = true;
			newState = PlayerState::MovingUp;
		}

		//If the player moves down
		if (yVel > 0)
		{
			changeState = true;
			newState = PlayerState::MovingDown;
		}
	}
	player->setAABB(collider);

	if (changeState == true) {

		player->setCurrentState(newState);
	}
}

void Player::MovingUp::move(Player * player, Box* box[], Tile *tiles[], Uint32 deltaTicks)
{
	AABB collider = player->getAABB();
	float xVel = player->getxVel();
	float yVel = player->getyVel();
	int WIDTH = player->collider.w;
	int HEIGHT = player->collider.h;

	bool changeState = false;
	PlayerState newState;
	

	//If the dot moves up
	if (yVel < 0)
	{
		collider.pos.y += yVel*(deltaTicks / 1000.f);
		//If the player moves too much
		if (collider.pos.y < 0)
		{
			//move back
			collider.pos.y = 0;
		}
		if (Tile::touches_wall(collider, tiles) )
		{
			collider.pos.y -= yVel*(deltaTicks / 1000.f);
		}
		for (int t = 0; t < 4; t++)
		{

			//If the collision box touches the box
			if (AABB::intersectAABB(&collider, &box[t]->getAABB())&& collider.pos.y>box[t]->getAABB().pos.y)
			{
				collider.pos.y = box[t]->getAABB().pos.y + 64;
			}

		}
	
	}
	if (yVel == 0)
	{
		//If the player moves to the right
		if (xVel > 0)
		{
			changeState = true;
			newState = PlayerState::MovingRight;
		}

		//If the player moves to the left
		if (xVel < 0)
		{
			changeState = true;
			newState = PlayerState::MovingLeft;
		}
	}
	//If the player moves down
	if (yVel > 0)
	{
		changeState = true;
		newState = PlayerState::MovingDown;
	}
	

	player->setAABB(collider);

	if (changeState == true) {

		player->setCurrentState(newState);
	}
}

void Player::MovingDown::move(Player * player, Box* box[], Tile *tiles[], Uint32 deltaTicks)
{
	AABB collider = player->getAABB();
	float xVel = player->getxVel();
	float yVel = player->getyVel();
	int WIDTH = player->collider.w;
	int HEIGHT = player->collider.h;

	bool changeState = false;
	PlayerState newState;
	
	//If the player moves down
	if (yVel > 0)
	{
		collider.pos.y += yVel*(deltaTicks / 1000.f);
		
		if (collider.pos.y + HEIGHT > Screen::SCREEN_HEIGHT)
		{
			//move back
			collider.pos.y = Screen::SCREEN_HEIGHT - HEIGHT;
		}

		if (Tile::touches_wall(collider, tiles) )
		{
			collider.pos.y -= yVel*(deltaTicks / 1000.f);
		}
		for (int t = 0; t < 4; t++)
		{

			//If the collision box touches the box
			if (AABB::intersectAABB(&collider, &box[t]->getAABB())&&collider.pos.y< box[t]->getAABB().pos.y)
			{
				collider.pos.y = box[t]->getAABB().pos.y - collider.h;
			}

		}
		
	}
	if (yVel == 0)
	{
		//If the player moves right
		if (xVel > 0)
		{
			changeState = true;
			newState = PlayerState::MovingRight;
		}

		//If the player moves left
		if (xVel < 0)
		{
			changeState = true;
			newState = PlayerState::MovingLeft;
		}
	}
	//If the player moves up
	if (yVel < 0)
	{
		changeState = true;
		newState = PlayerState::MovingUp;
	}
	

	player->setAABB(collider);

	if (changeState == true) {

		player->setCurrentState(newState);
	}
}



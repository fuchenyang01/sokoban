#pragma once
#include<SDL.h>
#include"Screen.h"
#include"TTF.h"
#include"Timer.h"
#include"Image.h"
#include <string>
#include <sstream>
#include"Player.h"
#include"Tile.h"
#include"Box.h"
#include"Dot.h"
#include"Button.h"
#include"Music.h"
#include"bgm.h"
class Game
{
private:
	//Make the timer
	Timer  delta;
	Timer timer;
	//The event structure
	SDL_Event event;
	Screen *window;
	bool quit;
	Player* player;
	Tile *tiles[70];
	Box* box[4];
	Dot* dot[4];
	Button* restart[2];

	const int FRAMES_PER_SECOND = 20;
	char* title = "Sokoban ";
	TTF* message[3];
	Music *music;

public:
	
	Game();
	
	~Game();
	//start the game
	bool start();

	
	bool init();
	//the game loop
	bool gameLoop();
	
	Screen * getScreen();
	//if win....
	bool win();
	bool getQuit();
	void freeMemory();
	
	
};



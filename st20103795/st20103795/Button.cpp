#include "Button.h"





Button::~Button()
{

}

Button::Button(float x, float y, char * filename, Uint8 R, Uint8 G, Uint8 B):Image(filename,R,G,B)
{
	box.x = x;
	box.y = y;
	box.w = 40;
	box.h = 40;
}

void Button::handle_events(SDL_Event event)
{
	//Mouse coordinates
	int x = 0, y = 0;

	//If the mouse has moved
	if (event.type == SDL_MOUSEMOTION)
	{
		//Get mouse coordinates
		x = event.motion.x;
		y = event.motion.y;

		//If the mouse is over the button
		if ((x > box.x) && (x < box.x + box.w) && (y > box.y) && (y < box.y + box.h))
		{
			setImage("restart2.png",0,0,0);
		}
		//if not
		else
		{
			setImage("restart.png", 0, 0, 0);
		}
	}
}

void Button::show(SDL_Surface*screen)
{
	//Display button
	apply_surface(box.x, box.y, getSurface(), screen);
}

SDL_Rect Button::getRect()
{
	return box;
}

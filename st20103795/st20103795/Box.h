#pragma once
#include"Tile.h"
#include"Music.h"
class Player;
class Box
{
	 Clips box;
	 AABB collider;
	 Music music;
public:
	Box(float x, float y);
	~Box();
	void show(SDL_Surface* screen);
	void move(Player* player, Box* box[], Tile*tiles[], Uint32 deltaTicks);
	AABB getAABB();
	bool notSameBox(AABB collider,Box* box[]);
	
	static bool touches_box(AABB collider, Box *box[]);
	bool box_box(Box* box[]);

};



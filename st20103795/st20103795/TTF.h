#pragma once
#include "SDL_ttf.h"
#include <sstream>
#include"Surface.h"

class TTF:public Surface
{
private:
	TTF_Font *font;
	SDL_Color Color;
	char* ttfname= "lazy.ttf";
	int ptsize;

public:
	TTF();
	TTF(const char* message,char*,int ,Uint8 R, Uint8 G, Uint8 B);
	~TTF();

	
	void setColor(Uint8 R, Uint8 G, Uint8 B);
	SDL_Color getColor();

	
	TTF_Font* getFont();
	
	void setFont(char* ttfname, int ptsize);

	void setTTF(const char* message);
	
	
	char* getTTFname();
	void setTTFname(char* name);

	int getPtsize();
	void setPtsize(int size);

};


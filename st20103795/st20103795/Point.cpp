#include "Point.h"

Point::Point() {

	x = y = 0;
}


Point::Point(const float initX, const float initY) {

	x = initX;
	y = initY;
}

Point::~Point()
{
	
}

// Accessor methods
float Point::getX() {

	return x;
}


float Point::getY() {

	return y;
}


void Point::setX(const float value) {

	x = value;
}


void Point::setY(const float value) {

	y = value;
}